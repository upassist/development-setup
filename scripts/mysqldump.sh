#! /bin/bash

TIMESTAMP=$(date +"%F")
BACKUP_DIR="./backup/$TIMESTAMP"
MYSQL_USER="root"
MYSQL_PASSWORD="password"

mkdir -p "$BACKUP_DIR/mysql"

databases=`mysql --user=$MYSQL_USER -p$MYSQL_PASSWORD -e "SHOW DATABASES;" | grep -Ev "(Database|information_schema|performance_schema)"`

for db in $databases; do
  mysqldump --force --opt --user=$MYSQL_USER -p$MYSQL_PASSWORD --databases $db | gzip > "$BACKUP_DIR/mysql/$db.gz"
done