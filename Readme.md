# Setup

- take a look at the different document roots in /sites
- set up dnsmasq to handle those postfixes
- make sure your docker-compose.yml contains the volume mappings and variables matching your setup
- run `docker-composer up -d`
- if you want to use phpdebug, uncomment the respective line and make sure you put in your local machine ip

# Docker Commands

command                             |   what it does
---                                 |   ---
`docker stop $(docker ps -q)`       |   stop all running docker containers
`docker-compose exec web /bin/zsh`  |   execute zsh shell inside the running container
`docker-composer up -d`             |
`docker-composer stop`              |
`docker-composer start`             |
`docker-composer restart`           |

# Todo

- better documentation
- php7 part
 

